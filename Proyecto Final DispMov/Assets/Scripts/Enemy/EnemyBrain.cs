﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBrain : MonoBehaviour
{
    public int health = 3;
    public GameObject target;

    private void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z), 5f * Time.deltaTime);
    }

    public void Damaged(int damage)
    {
        health = health - damage;
        if (health <= 0)
        {
            GameObject.FindGameObjectWithTag("Manager").GetComponent<Manger>().AddScore();
            Die();
        }
    }

    public void Die()
    {
        Destroy(gameObject);
    }

}
