// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/GunActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @GunActions : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @GunActions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""GunActions"",
    ""maps"": [
        {
            ""name"": ""Gameplay"",
            ""id"": ""dd5ac714-1b0c-4b39-a758-761339ec4165"",
            ""actions"": [
                {
                    ""name"": ""AimX"",
                    ""type"": ""Button"",
                    ""id"": ""f2a5dffc-bb18-4d10-bb8c-d8d55e4b6dbd"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""AimY"",
                    ""type"": ""Button"",
                    ""id"": ""f781c9ef-c1ef-4875-a2d1-5267c3aa2741"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Shoot"",
                    ""type"": ""Button"",
                    ""id"": ""c024cdf8-2a6f-430d-a71c-c9492c8359c9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Reload"",
                    ""type"": ""Button"",
                    ""id"": ""efc7dd6f-d1cc-40f0-9e9b-3461018ed5aa"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""9aa42141-9c1f-40eb-98be-708f51f52e11"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Shoot"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""37f322f2-43ad-44a4-867a-88ae257c2b29"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Reload"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""MovX"",
                    ""id"": ""e6cce826-64bb-4864-abc4-6a97206a801d"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AimX"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""a0e37b4b-cd74-46b7-adce-457133a34580"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AimX"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""bab39458-c897-4ad9-a57c-301d97582b84"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AimX"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""MovY"",
                    ""id"": ""fc8456d3-481f-4800-b4f3-3f01b980360e"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AimY"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""70bc1d57-6d26-46a1-840f-c8a6ae150e27"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AimY"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""5f77e23f-df76-4145-b91b-e404390d0d45"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AimY"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Gameplay
        m_Gameplay = asset.FindActionMap("Gameplay", throwIfNotFound: true);
        m_Gameplay_AimX = m_Gameplay.FindAction("AimX", throwIfNotFound: true);
        m_Gameplay_AimY = m_Gameplay.FindAction("AimY", throwIfNotFound: true);
        m_Gameplay_Shoot = m_Gameplay.FindAction("Shoot", throwIfNotFound: true);
        m_Gameplay_Reload = m_Gameplay.FindAction("Reload", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Gameplay
    private readonly InputActionMap m_Gameplay;
    private IGameplayActions m_GameplayActionsCallbackInterface;
    private readonly InputAction m_Gameplay_AimX;
    private readonly InputAction m_Gameplay_AimY;
    private readonly InputAction m_Gameplay_Shoot;
    private readonly InputAction m_Gameplay_Reload;
    public struct GameplayActions
    {
        private @GunActions m_Wrapper;
        public GameplayActions(@GunActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @AimX => m_Wrapper.m_Gameplay_AimX;
        public InputAction @AimY => m_Wrapper.m_Gameplay_AimY;
        public InputAction @Shoot => m_Wrapper.m_Gameplay_Shoot;
        public InputAction @Reload => m_Wrapper.m_Gameplay_Reload;
        public InputActionMap Get() { return m_Wrapper.m_Gameplay; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(GameplayActions set) { return set.Get(); }
        public void SetCallbacks(IGameplayActions instance)
        {
            if (m_Wrapper.m_GameplayActionsCallbackInterface != null)
            {
                @AimX.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnAimX;
                @AimX.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnAimX;
                @AimX.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnAimX;
                @AimY.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnAimY;
                @AimY.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnAimY;
                @AimY.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnAimY;
                @Shoot.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnShoot;
                @Shoot.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnShoot;
                @Shoot.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnShoot;
                @Reload.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnReload;
                @Reload.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnReload;
                @Reload.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnReload;
            }
            m_Wrapper.m_GameplayActionsCallbackInterface = instance;
            if (instance != null)
            {
                @AimX.started += instance.OnAimX;
                @AimX.performed += instance.OnAimX;
                @AimX.canceled += instance.OnAimX;
                @AimY.started += instance.OnAimY;
                @AimY.performed += instance.OnAimY;
                @AimY.canceled += instance.OnAimY;
                @Shoot.started += instance.OnShoot;
                @Shoot.performed += instance.OnShoot;
                @Shoot.canceled += instance.OnShoot;
                @Reload.started += instance.OnReload;
                @Reload.performed += instance.OnReload;
                @Reload.canceled += instance.OnReload;
            }
        }
    }
    public GameplayActions @Gameplay => new GameplayActions(this);
    public interface IGameplayActions
    {
        void OnAimX(InputAction.CallbackContext context);
        void OnAimY(InputAction.CallbackContext context);
        void OnShoot(InputAction.CallbackContext context);
        void OnReload(InputAction.CallbackContext context);
    }
}
