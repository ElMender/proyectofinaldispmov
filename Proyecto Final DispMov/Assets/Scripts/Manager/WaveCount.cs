using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveCount : MonoBehaviour
{
    public Text texto;
    public Manger manger;

    private void Start()
    {
        texto = GetComponent<Text>();
        manger = GameObject.FindGameObjectWithTag("Manager").GetComponent<Manger>();
    }

    private void Update()
    {
        texto.text = manger.CurrentLvl.ToString();
    }

}
