﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Manger : MonoBehaviour
{
    public int puntajeFinal;
    public Spawner spawn;
    public int CurrentLvl = 1;


    private void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    public void Reset()
    {
        puntajeFinal = 0;
        CurrentLvl = 1;
        SceneManager.LoadScene("MainMenu");
    }
    public void GameStart()
    {
        //puntajeFinal = null;
        //currentTurn = 1;
        //SceneManager.LoadScene("PrepareNext");
    }

    public void NextTurn()
    {
            EndGame();
    }

    public void DevueltaALaArena()
    {
        SceneManager.LoadScene("Lvl1");
    }

    public void EndGame()
    {
        SceneManager.LoadScene("FinalScores");
    }

    public void AddScore()
    {
        puntajeFinal++;
        if (puntajeFinal == 5 || puntajeFinal == 10 || puntajeFinal == 15)
        {
            CurrentLvl++;
        }
    }

}
