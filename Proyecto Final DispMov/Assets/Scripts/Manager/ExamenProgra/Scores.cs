﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public struct Scores
{
    public string name;
    public int score;

    public Scores(string name, int score)
    {
        this.name = name;
        this.score = score;
    }

}
