﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayScore : MonoBehaviour
{
    public int place;
    Text texto;

    RectTransform estaPos;
    public RectTransform lugarTexto;
    private void Awake()
    {
        estaPos = GetComponent<RectTransform>();
        texto = GetComponent<Text>();
        texto.text = "Total score: " + GameObject.FindGameObjectWithTag("Manager").GetComponent<Manger>().puntajeFinal;
        estaPos.position = lugarTexto.position;
    }

}
