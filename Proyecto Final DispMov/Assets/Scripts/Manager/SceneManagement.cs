using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagement : MonoBehaviour
{

    public void GameStart()
    {
        SceneManager.LoadScene("Lvl1");
    }

    public void GameOver()
    {
        SceneManager.LoadScene("FinalScores");
    }

}
