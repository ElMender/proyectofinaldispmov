﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public GameObject enemyPrefab;
    public GameObject Parent;
    public Manger manger;

    Vector3 FinalCoords;

    int VarX;
    int VarZ;

    private void Awake()
    {
        Parent = GameObject.Find("Rotoscope");
        StartCoroutine(Spawn());
    }

    IEnumerator Spawn()
    {
        //for (int i = 0; i < GameObject.FindGameObjectWithTag("Manager").GetComponent<Manger>().CurrentLvl * 5; i++)
        //{
        while (true)
        {
            if (GameObject.FindGameObjectWithTag("Manager").GetComponent<Manger>().CurrentLvl == 1)
            {
                yield return new WaitForSeconds(3f);
            }

            if (GameObject.FindGameObjectWithTag("Manager").GetComponent<Manger>().CurrentLvl == 2)
            {
                yield return new WaitForSeconds(2f);
            }

            if (GameObject.FindGameObjectWithTag("Manager").GetComponent<Manger>().CurrentLvl == 3)
            {
                yield return new WaitForSeconds(1f);
            }

            VarX = Random.Range(-15, 15);
            VarZ = Random.Range(-5, 5);
            FinalCoords = new Vector3(transform.position.x + VarX, 1f, transform.position.z + VarZ);

            SpawnearEnemigo();
        }
        //}
    }

    void SpawnearEnemigo()
    {
        Instantiate(enemyPrefab,FinalCoords,Quaternion.identity,Parent.transform);

    }

}
