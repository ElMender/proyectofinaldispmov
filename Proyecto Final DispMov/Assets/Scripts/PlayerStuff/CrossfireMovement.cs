﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CrossfireMovement : MonoBehaviour
{
    GunActions actions;
    public float speed;
    private Vector3 screenbounds;
    public int damage = 1;

    public AudioSource shoot;

    public Transform camara, crossfire;

    private void Awake()
    {
        actions = new GunActions();
    }

    private void Start()
    {
        screenbounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
    }

    private void OnEnable()
    {
        actions.Enable();
    }

    private void OnDisable()
    {
        actions.Disable();
    }

    void Update()
    {

        float velX = actions.Gameplay.AimX.ReadValue<float>();
        float velY = actions.Gameplay.AimY.ReadValue<float>();
        transform.localPosition = new Vector3(Mathf.Clamp(transform.localPosition.x,screenbounds.x,screenbounds.x * -1) + velX * speed * Time.deltaTime, Mathf.Clamp(transform.localPosition.y,screenbounds.y+ 1 *-1 ,screenbounds.y + 1) + velY * speed * Time.deltaTime, transform.localPosition.z);

    }

    public void Disparo()
    {
        RaycastHit hit;

        shoot.Play();

        if (Physics.Raycast(camara.position, crossfire.position - camara.position, out hit, 100f))
        {
            Debug.Log(hit.transform.name);

            EnemyBrain target = hit.transform.GetComponent<EnemyBrain>();
            if (target != null)
            {
                target.Damaged(damage);
            }
        }

    }
}
