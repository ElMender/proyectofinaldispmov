using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flash : MonoBehaviour
{

    public Animator flash;

    public void Flashing()
    {
        flash.SetTrigger("flash");
    }

}
